<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demodb' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3+H_30zn+Z`H=xc&z/Q%Zq |Jyo[kK$h^7) d)R:sJoE_Rei@l9|^UaKm?pe.yEw' );
define( 'SECURE_AUTH_KEY',  'W[[(OjyU%XUXW,B6Q:E6rK})qg-YCVbk;&s+P%cj2y34r3ys]KqFtVMG9>n)vZ2p' );
define( 'LOGGED_IN_KEY',    '{g;08:XfMhv.P2oe`UyBlMqv?au^4)K%<{PvzMH-X1K!7*Qxq#ccP0co> ryKV!Z' );
define( 'NONCE_KEY',        '<jDd/uCc~<|u}adZxn7wxXWo?RqUdq@wDf9&zT&I JWGE.;pi3tu7@j*kth4|_t{' );
define( 'AUTH_SALT',        '07&l#8_y4=3zn@%M93X{SZ`?]<=@?v^meg^;o2B2/<C=s*VI05Qp7n}l2SqMwDHs' );
define( 'SECURE_AUTH_SALT', '~D//j6O3vA&]IE:_7~;Vd*]2 4utKR-<GfMSWki/ezssRV[|$<5}]TVtBc9K=%=Q' );
define( 'LOGGED_IN_SALT',   'pUS,,dn4sVu3?!>0soks{s!8b^EfvgF.A)]?nrU$3:>0xvA_C]M)E*Zzimc87W7(' );
define( 'NONCE_SALT',       't$jx]h?)YAsY>@$>%Ct@bmj;;X8CDetrDPr5BFgb#WUtLjv#.lRg-8i4TRLpH Ev' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
