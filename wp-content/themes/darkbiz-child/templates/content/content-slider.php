<?php 
/**
 * Template part for displaying slider in pages
 *
 * @since 1.0.0
 * 
 * @package Darkbiz WordPress Theme
 */

//$pst = Darkbiz_Theme::get_posts_by_type( darkbiz_get( 'slider-type' ), darkbiz_get( 'cat-post' ), 3 );
//if( !empty( $pst ) ):?>
	<div class="darkbiz-banner-slider-wrapper"> 
		<div class="darkbiz-banner-slider-init">
			<?php
			$pst = array(
				get_stylesheet_directory_uri().'/images/slide1.jpg',
				get_stylesheet_directory_uri().'/images/slide2.jpg',
				get_stylesheet_directory_uri().'/images/slide3.jpg',

			);
			foreach( $pst as $p ): 
				if(  $p ){
					$img = $p;
				}else{
					$img = get_template_directory_uri() . '/assets/img/default-banner.jpg';
				}?>
				<div class="darkbiz-banner-slider-inner" style="background-image: url( <?php echo esc_url( $img ) ?>)">
					<div class="banner-slider-content">
						<h2>
							<a href="#">								
								<?php echo 'Lorem ipsum dolor sit amet'; ?>
							</a>
						</h2>
						<p class="feature-news-content"><?php echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt'; ?></p>
						
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php //endif; ?>
